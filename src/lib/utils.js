import config from 'config';
import fs from 'fs';
import path from 'path';
import md5File from 'md5-file';
import xml2js from 'xml2js';

export function uniqueID() {
  function chr4() {
    return Math.random().toString(16).slice(-4);
  }

  return chr4() + chr4() +
    '-' + chr4() +
    '-' + chr4() +
    '-' + chr4() +
    '-' + chr4() + chr4() + chr4();
}

// Traitement des ROMs
export async function getRoms(system, subpath = '') {
  const srcpath = path.join(config.get('recalbox.romsPath'), system, subpath);
  const esSystems = await getEsSystems();
  const systemData = esSystems.find(s => s.name === system);
  const romExtensions = systemData ? systemData.extensions : [];

  return fs.readdirSync(srcpath).filter((file) => {
    return fs.statSync(path.join(srcpath, file)).isFile() &&
      -1 !== romExtensions.indexOf(path.extname(file));
  });
}

// Promise xml2json
async function xmlToJson(file) {
  var promise = await new Promise((resolve, reject) => {
    const parser = new xml2js.Parser({
      trim: true,
      explicitArray: false
    });

    parser.parseString(fs.readFileSync(file), (jsError, jsResult) => {
      if (jsError) {
        reject(jsError);
      } else {
        resolve(jsResult);
      }
    });
  });

  return promise;
}

// Traitements des systèmes supportés par ES
let esSystems;

export async function getEsSystems() {
  if (esSystems) {
    return esSystems;
  }

  const json = await xmlToJson(config.get('recalbox.esSystemsCfgPath'));
  esSystems = [];

  esSystems.push({
    launchCommand: json.systemList.defaults.$.command
  })

  json.systemList.system.forEach((system) => {
    esSystems.push({
      name: system.$.name,
      fullname: system.$.fullname,
      path: system.descriptor.$.path,
      extensions: system.descriptor.$.extensions ? system.descriptor.$.extensions.split(' ') : [],
    });
  });

  return esSystems;
}

// Traitement des fichiers gamelist.xml
export function parseGameReleaseDate(releaseDate) {
  return {
    day: releaseDate.substring(6, 8),
    month: releaseDate.substring(4, 6),
    year: releaseDate.substring(0, 4),
  };
}

export function getSystemRomsBasePath(system) {
  return path.join(config.get('recalbox.romsPath'), system);
}

export function getSystemGamelistPath(system) {
  return path.join(getSystemRomsBasePath(system), 'gamelist.xml');
}

export async function getSystemGamelist(system, raw = false) {
  const gameListPath = getSystemGamelistPath(system);

  if (!fs.existsSync(gameListPath)) {
    return {};
  }

  const json = await xmlToJson(gameListPath);

  if (raw) {
    return json;
  }

  let list = {};
  let gameList = json.gameList.game || [];

  if (!Array.isArray(gameList)) {
    gameList = [gameList];
  }

  gameList.forEach((game) => {
    list[game.path.substring(2)] = game;
  });

  return list;
}
